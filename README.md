# aliddns

#### 介绍
获取运行所在主机的公网地址,解析到阿里域名上.
使用场景主要还是家里或公司部署的nas ftp 测试系统 之类的.

 **只更新A记录,不更新TXT CNAME 等类型的IP** 

#### 软件架构
使用阿里SDK 和 阿里appid secret更新域名解析.
没写什么层级就2个类,实际上就一个类.



#### 使用说明
APPID          阿里APPID
SECRET         阿里SECRET
SUB.DOMAIN.COM 自己在阿里域名解析配置的二级域名

1.  修改aliddns.bat里的APPID SECRET SUB.DOMAIN.COM,比如 nas.xx.com,然后在域名运营商那里先配置一个默认的IP.

2.  阿里appid 和secret从这里获取https://ak-console.aliyun.com/#/accesskey  ,但是一般建议RAM角色来控制资源.因为前面那种方式获取的appid是全部权限的,RAM角色可以控制只开通dns的权限.

3.  如果能更新你域名的解析后,就可以打电信运营商电话开通公网IP,说你需要远程家里的电脑.

4.  家里路由器上设置一下端口转发,搞定.

![输入图片说明](https://images.gitee.com/uploads/images/2019/1019/162610_7b63a195_10325.png "1.png")
