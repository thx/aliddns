package com.uapdev.aliddns;

/**
 * 启动监听,默认20秒获取一次当前IP,10分钟获取一次阿里配置的A记录IP,不相同则更新阿里IP
 *args[0] 阿里appid
 *args[1] 阿里secret
 *args[2] 阿里子域名  例如 sub.domain.com
 */
public class App {
	public static void main(String[] args) {
//		new AliMonitor("appid","secret").start("sub.domain.com");
		if(args == null || args.length < 3) {
			System.err.println("args error , please see wiki");
			return;
		}
		new AliMonitor(args[0],args[1]).start(args[2]);
		
	}
}
